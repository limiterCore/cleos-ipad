import * as types from './actionTypes';
import girlsApi from '../api/girlsApi';
import { beginAjaxCall } from './ajaxStatusActions';

// Load girls
export function loadGirlsSuccess(girls) {
	return { type: types.LOAD_GIRLS_SUCCESS, girls};
}

export function loadGirls(token) {
	return function(dispatch) {
		dispatch(beginAjaxCall());
		return girlsApi.getAllGirls(token).then(girls => {
			dispatch(loadGirlsSuccess(girls));
		}).catch(error => {
			throw(error);
		});
	};
}

// Open girl
export function openGirl() {
	return { type: types.OPEN_GIRL };
}

// Open sidebar
export function openSidebar() {
	return { type: types.OPEN_SIDEBAR };
}

// Close sidebar
export function closeSidebar() {
	return { type: types.CLOSE_SIDEBAR };
}