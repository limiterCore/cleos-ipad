import * as types from './actionTypes';

// Action for updating filter
export function updateFilter(filter, name) {
	return { type: types.CHANGE_FILTER, payload: {
		filter,
		name,
	}};
}

// Action for updating range filter
export function updateRangeFilter(filter, value) {
	return { type: types.CHANGE_RANGE_FILTER, payload: {
		filter,
		value,
	}};
}

// Action for updating name filter
export function updateNameFilter(value) {
	return { type: types.CHANGE_NAME_FILTER, payload: {
		value,
	}};
}

// Action for reseting filters
export function resetAllFilters(filter) {
	return { type: types.RESET_FILTER };
}

// Action for updating girls
export function updateGirls(filter) {
	return { type: types.FILTER_GIRLS, payload: {
		filter,
	}};
}

// Change filter
export function changeFilter(filter, name) {
	return function(dispatch, getState) {
		dispatch(updateFilter(filter, name));
		let { filter: stateFilter } = getState();
		dispatch(updateGirls(stateFilter));
	};
}

// Change range filter
export function changeRangeFilter(filter, value) {
	return function(dispatch, getState) {
		dispatch(updateRangeFilter(filter, value));
		let { filter: stateFilter } = getState();
		dispatch(updateGirls(stateFilter));
	};
}

// Change name filter
export function changeNameFilter(value) {
	return function(dispatch, getState) {
		dispatch(updateNameFilter(value));
		let { filter: stateFilter } = getState();
		dispatch(updateGirls(stateFilter));
	};
}

// Reset filter
export function resetFilter(value) {
	return function(dispatch, getState) {
		dispatch(resetAllFilters());
		let { filter: stateFilter } = getState();
		dispatch(updateGirls(stateFilter));
	};
}