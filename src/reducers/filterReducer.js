import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function filterReducer(state = initialState.filter, action) {
	switch(action.type) {

	case types.LOAD_GIRLS_SUCCESS:
		let dressSize = {
			min: 1000,
			max: 0,
			from: 1000,
			to: 0,
		};

		let age = {
			min: 1000,
			max: 0,
			from: 1000,
			to: 0,
		};

		let height = {
			min: 1000,
			max: 0,
			from: 1000,
			to: 0,
		};

		let bustSize = [];
		let nationality = [];
		let hairColour = [];
		let eyeColour = [];
		let skinColour = [];
		// let services = [];
		let services = [];
		let flags = [
			{
				name: 'New only',
				active: false,
			},
			{
				name: 'Works now',
				active: false,
			}
		];

		action.girls.forEach(item => {
			// Dress size
			if (parseInt(item['wpcf-dress'], 10) < dressSize.min) {
				dressSize.min = parseInt(item['wpcf-dress'], 10);
				dressSize.from = dressSize.min;
			}
			if (parseInt(item['wpcf-dress'], 10) > dressSize.max) {
				dressSize.max = parseInt(item['wpcf-dress'], 10);
				dressSize.to = dressSize.max;
			}

			// Age
			if (parseInt(item['wpcf-age'], 10) < age.min) {
				age.min = parseInt(item['wpcf-age'], 10);
				age.from = age.min;
			}
			if (parseInt(item['wpcf-age'], 10) > age.max) {
				age.max = parseInt(item['wpcf-age'], 10);
				age.to = age.max;
			}

			// Height
			if (parseInt(item['wpcf-height'], 10) < height.min) {
				height.min = parseInt(item['wpcf-height'], 10);
				height.from = height.min;
			}
			if (parseInt(item['wpcf-height'], 10) > height.max) {
				height.max = parseInt(item['wpcf-height'], 10);
				height.to = height.max;
			}

			// Bust size
			if (!bustSize.find(el => el.name === item['wpcf-bust-size'])) {
				if (item['wpcf-bust-size'] !== '') {
					bustSize.push({
						name: item['wpcf-bust-size'],
						filterName: 'wpcf-bust-size',
						active: false,
					});
				}
			}

			// Nationality
			if (!nationality.find(el => el.name === item['wpcf-nationality'])) {
				if (item['wpcf-nationality'] !== '') {
					nationality.push({
						name: item['wpcf-nationality'],
						filterName: 'wpcf-nationality',
						active: false,
					});
				}
			}

			// Hair colour
			if (!hairColour.find(el => el.name === item['wpcf-hair-colour'])) {
				if (item['wpcf-hair-colour'] !== '') {
					hairColour.push({
						name: item['wpcf-hair-colour'],
						filterName: 'wpcf-hair-colour',
						active: false,
					});
				}
			}

			// Eye colour
			if (!eyeColour.find(el => el.name === item['wpcf-eye-colour'])) {
				if (item['wpcf-eye-colour'] !== '') {
					eyeColour.push({
						name: item['wpcf-eye-colour'],
						filterName: 'wpcf-eye-colour',
						active: false,
					});
				}
			}

			// Skin colour
			if (!skinColour.find(el => el.name === item['wpcf-skin-colour'])) {
				if (item['wpcf-skin-colour'] !== '') {
					skinColour.push({
						name: item['wpcf-skin-colour'],
						filterName: 'wpcf-skin-colour',
						active: false,
					});
				}
			}

			// Services
			/* if (item.sex_services2 && typeof item.sex_services2 !== 'string' && item.sex_services2.length > 0) {
				item.sex_services2.forEach(serv => {
					if (!services.find(el => el.name === serv)) {
						services.push({
							name: serv,
							filterName: 'services',
							active: false,
						});
					}
				});
			} */

			// New services
			if (item.services_for_digital_menu) {
				item.services_for_digital_menu.split(',').map(serv => serv.trim()).forEach(serv => {
					if (!services.find(el => el.name === serv)) {
						services.push({
							name: serv,
							filterName: 'services',
							active: false,
						});
					}
				});
			}
		});

		return {
			dressSize,
			height,
			age,
			bustSize,
			nationality: nationality.sort((a,b) => {
				if (a.name === b.name) {
					return 0;
				}
				return a.name > b.name ? 1 : -1;
			}),
			hairColour: hairColour.sort((a,b) => {
				if (a.name === b.name) {
					return 0;
				}
				return a.name > b.name ? 1 : -1;
			}),
			eyeColour: eyeColour.sort((a,b) => {
				if (a.name === b.name) {
					return 0;
				}
				return a.name > b.name ? 1 : -1;
			}),
			skinColour: skinColour.sort((a,b) => {
				if (a.name === b.name) {
					return 0;
				}
				return a.name > b.name ? 1 : -1;
			}),
			services: services.sort((a,b) => {
				if (a.name === b.name) {
					return 0;
				}
				return a.name > b.name ? 1 : -1;
			}),
			flags,
			name: '',
		};

	case types.CHANGE_FILTER: {
		let newFilter = JSON.parse(JSON.stringify(state));
		let index = newFilter[action.payload.filter].findIndex(el => el.name === action.payload.name);

		if (index > -1) {
			let active = newFilter[action.payload.filter][index].active;
			newFilter[action.payload.filter][index].active = !active;
		}

		return newFilter;
	}

	case types.CHANGE_RANGE_FILTER: {
		let newFilter = JSON.parse(JSON.stringify(state));

		newFilter[action.payload.filter].from = action.payload.value.min;
		newFilter[action.payload.filter].to = action.payload.value.max;

		return newFilter;
	}

	case types.CHANGE_NAME_FILTER: {
		let newFilter = JSON.parse(JSON.stringify(state));

		newFilter.name = action.payload.value;

		return newFilter;
	}

	case types.RESET_FILTER: {
		let newFilter = JSON.parse(JSON.stringify(state));

		// Reset age
		newFilter.age.from = newFilter.age.min;
		newFilter.age.to = newFilter.age.max;

		// Reset height
		newFilter.height.from = newFilter.height.min;
		newFilter.height.to = newFilter.height.max;

		// Reset dress size
		newFilter.dressSize.from = newFilter.dressSize.min;
		newFilter.dressSize.to = newFilter.dressSize.max;

		// Reset name
		newFilter.name = '';

		// Reset bust size
		newFilter.bustSize.forEach((item, index) => {
			newFilter.bustSize[index].active = false;
		});

		// Reset nationality
		newFilter.nationality.forEach((item, index) => {
			newFilter.nationality[index].active = false;
		});

		// Reset hairColour
		newFilter.hairColour.forEach((item, index) => {
			newFilter.hairColour[index].active = false;
		});

		// Reset eyeColour
		newFilter.eyeColour.forEach((item, index) => {
			newFilter.eyeColour[index].active = false;
		});

		// Reset skinColour
		newFilter.skinColour.forEach((item, index) => {
			newFilter.skinColour[index].active = false;
		});

		// Reset services
		newFilter.services.forEach((item, index) => {
			newFilter.services[index].active = false;
		});

		// Reset new services
		newFilter.services.forEach((item, index) => {
			newFilter.services[index].active = false;
		});

		// Reset flags
		newFilter.flags.forEach((item, index) => {
			newFilter.flags[index].active = false;
		});

		return newFilter;
	}


	default:
		return state;
	}
}
