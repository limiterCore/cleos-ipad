import { combineReducers } from 'redux';
import girls from './girlsReducer';
import filter from './filterReducer';
import ajaxCallsInProgress from './ajaxStatusReducer';
import sidebarOpen from './sidebarOpenReducer';

const rootReducer = combineReducers({
	girls,
	filter,
	ajaxCallsInProgress,
	sidebarOpen,
});

export default rootReducer;