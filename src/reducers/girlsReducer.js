import * as types from '../actions/actionTypes';
import initialState from './initialState';

export default function girlsReducer(state = initialState.girls, action) {
	switch(action.type) {

	case types.LOAD_GIRLS_SUCCESS:
		return action.girls;

	case types.FILTER_GIRLS:
		let girls = JSON.parse(JSON.stringify(state));
		let filter = action.payload.filter;

		let newGirls;

		// Reset
		girls.forEach((el, elIndex) => {
			girls[elIndex].hidden = false;
		});

		// Bust size
		let actives = [];
		filter.bustSize.forEach(fitem => {
			if (fitem.active) {
				actives.push(fitem.name);
			}
		});
		if (actives.length > 0) {
			girls.forEach((el, elIndex) => {
				let index = actives.indexOf(el['wpcf-bust-size']);
				girls[elIndex].hidden = index === -1;
			});

			newGirls = [...girls];
		}
		else {
			girls.forEach((el, elIndex) => {
				girls[elIndex].hidden = false;
			});
			newGirls = [...girls];
		}


		// Nationality
		actives = [];
		filter.nationality.forEach(fitem => {
			if (fitem.active) {
				actives.push(fitem.name);
			}
		});
		if (actives.length > 0) {
			newGirls.forEach((el, elIndex) => {
				let index = actives.indexOf(el['wpcf-nationality']);
				if (index === -1) {
					newGirls[elIndex].hidden = true;
				}
			});
		}

		// Hair colour
		actives = [];
		filter.hairColour.forEach(fitem => {
			if (fitem.active) {
				actives.push(fitem.name);
			}
		});
		if (actives.length > 0) {
			newGirls.forEach((el, elIndex) => {
				let index = actives.indexOf(el['wpcf-hair-colour']);
				if (index === -1) {
					newGirls[elIndex].hidden = true;
				}
			});
		}

		// Eye colour
		actives = [];
		filter.eyeColour.forEach(fitem => {
			if (fitem.active) {
				actives.push(fitem.name);
			}
		});
		if (actives.length > 0) {
			newGirls.forEach((el, elIndex) => {
				let index = actives.indexOf(el['wpcf-eye-colour']);
				if (index === -1) {
					newGirls[elIndex].hidden = true;
				}
			});
		}

		// Skin colour
		actives = [];
		filter.skinColour.forEach(fitem => {
			if (fitem.active) {
				actives.push(fitem.name);
			}
		});
		if (actives.length > 0) {
			newGirls.forEach((el, elIndex) => {
				let index = actives.indexOf(el['wpcf-skin-colour']);
				if (index === -1) {
					newGirls[elIndex].hidden = true;
				}
			});
		}

		// Services
		/* actives = [];
		filter.services.forEach(fitem => {
			if (fitem.active) {
				actives.push(fitem.name);
			}
		});
		if (actives.length > 0) {
			newGirls.forEach((el, elIndex) => {

				// Hide if there are no services
				if (el.sex_services2.length < 1) {
					newGirls[elIndex].hidden = true;
				}
				else {
					let hidden = true;
					el.sex_services2.forEach(serv => {
						// if (serv.active) {
							let index = actives.indexOf(serv);
							if (index > -1) {
								hidden = false;
							}
						// }
					});

					newGirls[elIndex].hidden = hidden;
				}
			});
		} */

		// Services
		actives = [];
		filter.services.forEach(fitem => {
			if (fitem.active) {
				actives.push(fitem.name);
			}
		});
		if (actives.length > 0) {
			newGirls.forEach((el, elIndex) => {

				// Hide if there are no services
				if (el.services_for_digital_menu.trim().length < 1) {
					newGirls[elIndex].hidden = true;
				}
				else {
					let hidden = true;
					el.services_for_digital_menu.split(',').map(serv => serv.trim()).forEach(serv => {
						// if (serv.active) {
							let index = actives.indexOf(serv);
							if (index > -1) {
								hidden = false;
							}
						// }
					});

					newGirls[elIndex].hidden = hidden;
				}
			});
		}

		// Flags
		filter.flags.forEach(fitem => {
			if (fitem.active) {
				if (fitem.name === 'New only') {
					newGirls.forEach((el, elIndex) => {
						if (!el['wpcf-new']) {
							newGirls[elIndex].hidden = true;
						}
					});
				}

				if (fitem.name === 'Works now') {
					newGirls.forEach((el, elIndex) => {
						if (!el['works-now']) {
							newGirls[elIndex].hidden = true;
						}
					});
				}
			}
		});

		// Age
		newGirls.forEach((el, elIndex) => {
			let age = parseInt(el['wpcf-age'], 10);
			if (age < filter.age.from || age > filter.age.to) {
				newGirls[elIndex].hidden = true;
			}
		});

		// Height
		newGirls.forEach((el, elIndex) => {
			let height = parseInt(el['wpcf-height'], 10);
			if (height < filter.height.from || height > filter.height.to) {
				newGirls[elIndex].hidden = true;
			}
		});

		// Dress size
		newGirls.forEach((el, elIndex) => {
			let dressSize = parseInt(el['wpcf-dress'], 10);
			if (dressSize < filter.dressSize.from || dressSize > filter.dressSize.to) {
				newGirls[elIndex].hidden = true;
			}
		});

		// Name
		if (filter.name !== '') {
			newGirls.forEach((el, elIndex) => {
				if (el.title.rendered.toLowerCase().indexOf(filter.name.toLowerCase()) === -1) {
					newGirls[elIndex].hidden = true;
				}
			});
		}

		return newGirls;


	default:
		return state;
	}
}
