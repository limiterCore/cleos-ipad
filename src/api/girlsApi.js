import axios from 'axios';
import { API_PATH } from '../config';

class GirlsApi {

	static getAllGirls() {
		return new Promise((resolve, reject) => {
			axios.get(API_PATH + 'lady', {
				params: {
					per_page: 100,
					orderby: 'title',
					order: 'asc',
				}
			}).then(res => {
				resolve(res.data);
			});
		});
	}

	static getGirl(girlId) {
		girlId = parseInt(girlId, 10);
		return new Promise((resolve, reject) => {
			axios.get(API_PATH + 'lady/' + girlId)
				.then(res => {
					resolve(res.data);
				});
		});
	}
}

export default GirlsApi;