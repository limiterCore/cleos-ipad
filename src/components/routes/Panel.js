// Core
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory } from 'react-router';

// Actions
import * as girlActions from '../../actions/girlActions';
import * as filterActions from '../../actions/filterActions';

// Components
import LoadingSpinner from '../shared/LoadingSpinner';
import { RouteTransition } from 'react-router-transition';

// Misc
import classNames from 'classnames';
import InputRange from 'react-input-range';

// CSS
import '../../css/main.less';

class Panel extends Component {
	constructor(props) {
		super(props);

		this.state = {
			sidebarOpen           : false,
			ageValue              : {},
			heightValue           : {},
			dressSizeValue        : {},
			initialPropsReceiving : false,
			searchValue           : '',
		};

		this.props.girlActions.loadGirls();

		this.openSidebar = this.openSidebar.bind(this);
		this.closeSidebar = this.closeSidebar.bind(this);

		this.changeSearchValue = this.changeSearchValue.bind(this);
		this.eraseSearch = this.eraseSearch.bind(this);
		this.resetFilters = this.resetFilters.bind(this);
	}

	openSidebar() {
		this.props.girlActions.openSidebar();
		this.setState({
			sidebarOpen: true,
		});
	}

	closeSidebar() {
		this.props.girlActions.closeSidebar();
		this.setState({
			sidebarOpen: false,
		});
	}

	changeFilter(filter, name) {
		this.props.filterActions.changeFilter(filter, name);
	}

	changeRangeFilter(filter, value, updateFilter = false) {
		let obj = {};
		obj[filter] = value;
		this.setState(obj);

		if (updateFilter) {
			this.props.filterActions.changeRangeFilter(filter, value);
		}
	}

	resetFilters() {
		this.setState({
			age: Object.assign({}, this.props.filter.age),
			height: Object.assign({}, this.props.filter.height),
			dressSize: Object.assign({}, this.props.filter.dressSize),
			searchValue: '',
		});

		this.props.filterActions.resetFilter();
	}

	changeSearchValue(e) {
		this.setState({
			searchValue: e.target.value,
		});

		this.props.filterActions.changeNameFilter(e.target.value);
	}

	eraseSearch() {
		if (this.state.searchValue !== '') {
			this.setState({
				searchValue: '',
			});

			this.props.filterActions.changeNameFilter('');
		}
	}

	componentWillReceiveProps(nextProps) {
		if (nextProps.filter !== this.props.filter && !this.state.initialPropsReceiving) {
			let { filter } = nextProps;
			this.setState({
				age: Object.assign({}, filter.age),
				height: Object.assign({}, filter.height),
				dressSize: Object.assign({}, filter.dressSize),
				initialPropsReceiving: true,
			});
		}
		if (nextProps.sidebarOpen !== this.state.sidebarOpen) {
			this.setState({
				sidebarOpen: nextProps.sidebarOpen,
			});
		}
	}

	render() {
		let sidebarClass = classNames({
			"main-sidebar": true,
			"open"        : this.state.sidebarOpen,
		});

		let filter = this.props.filter;

		let popStateStyles = {
			atEnter: {
				translateX: -100,
				opacity: 0,
			},
			atLeave: {
				translateX: 100,
				opacity: 0,
			},
			atActive: {
				translateX: 0,
				opacity: 1,
			},
			mapStyles: styles => ({
				transform: `translateX(${styles.translateX}%)`,
				opacity: styles.opacity,
			}),
			className: classNames({
				"main-content-wrapper": true,
				"main-content-wrapper_shifted": this.state.sidebarOpen,
			}),
		};

		let pushStateStyles = {
			atEnter: {
				translateX: 100,
				opacity: 0,
			},
			atLeave: {
				translateX: -100,
				opacity: 0,
			},
			atActive: {
				translateX: 0,
				opacity: 1,
			},
			mapStyles: styles => ({
				transform: `translateX(${styles.translateX}%)`,
				opacity: styles.opacity,
			}),
			className: classNames({
				"main-content-wrapper": true,
				"main-content-wrapper_shifted": this.state.sidebarOpen,
			}),
		};

		let transitionStyles = this.props.location.action === 'POP'
			? popStateStyles
			: pushStateStyles;

		return (
			<div className="all-wrap">
				<aside
					className={sidebarClass}
				>
					<div className="main-sidebar__inactive">
						{this.props.location.pathname === '/' &&
						<button
							className="main-sidebar__open-filters"
							onClick={this.openSidebar}
						/>
						}

						{this.props.location.pathname !== '/' &&
						<button
							className="main-sidebar__back"
							onClick={browserHistory.goBack}
						>

						</button>
						}
					</div>

					{/*<button
						className="main-sidebar__close"
					    onClick={this.closeSidebar}
					/>*/}

					{/*FILTERS*/}
					<div className="filters">

						{/*SEARCH*/}
						<div
							className={classNames({
								"filters__search": true,
								"filled"         : this.state.searchValue !== '',
							})}
						>
							<div className="filters__search-input-holder">
								<input
									type="text"
									className="filters__search-input"
									placeholder="Search a lady by Name"
								    value={this.state.searchValue}
								    onChange={this.changeSearchValue}
								/>
							</div>
							<button
								className="filters__search-button"
							    onClick={this.eraseSearch}
							/>
						</div>
						{/*/SEARCH*/}

						{/*AGE*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Age</h3>
							{/*<a href="#" className="filters__section-reset js-reset-range"/>*/}

							{Boolean(filter) && Boolean(filter.age) &&
							<div className="filters__range-container" data-min={filter.age.min} data-max={filter.age.max}>
								<InputRange
									minValue={filter.age.min}
									maxValue={filter.age.max}
									value={this.state.age}
									onChange={value => this.changeRangeFilter('age', value)}
									onChangeComplete={value => this.changeRangeFilter('age', value, true)}
								/>
							</div>
							}
						</div>
						{/*/AGE*/}

						{/*BUST SIZE*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Bust</h3>

							<div className="filters__options-container">
								{Boolean(filter) && Boolean(filter.bustSize) && filter.bustSize.map((item, index) => (
									<div
										className={classNames({
											"filters__option": true,
											"active": item.active,
										})}
										key={index}
									    onClick={this.changeFilter.bind(this, "bustSize", item.name)}
									>
										{item.name}
									</div>
								))}
							</div>
						</div>
						{/*/BUST SIZE*/}

						{/*HEIGHT*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Height</h3>
							{/*<a href="#" className="filters__section-reset js-reset-range"/>*/}

							{Boolean(filter) && Boolean(filter.height) &&
							<div className="filters__range-container" data-min={filter.height.min} data-max={filter.height.max}>
								<InputRange
									minValue={filter.height.min}
									maxValue={filter.height.max}
									value={this.state.height}
									onChange={value => this.changeRangeFilter('height', value)}
									onChangeComplete={value => this.changeRangeFilter('height', value, true)}
								/>
							</div>
							}
						</div>
						{/*/HEIGHT*/}

						{/*DRESS SIZE*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Dress size</h3>
							{/*<a href="#" className="filters__section-reset js-reset-range"/>*/}

							{Boolean(filter) && Boolean(filter.dressSize) &&
							<div className="filters__range-container" data-min={filter.dressSize.min} data-max={filter.dressSize.max}>
								<InputRange
									minValue={filter.dressSize.min}
									maxValue={filter.dressSize.max}
									value={this.state.dressSize}
									onChange={value => this.changeRangeFilter('dressSize', value)}
									onChangeComplete={value => this.changeRangeFilter('dressSize', value, true)}
								/>
							</div>
							}
						</div>
						{/*/DRESS SIZE*/}

						{/*NATIONALITY*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Nationality</h3>
							{/*<a href="#" className="filters__section-reset js-reset-checkbox"/>*/}

							<div className="filters__options-container">
								{Boolean(filter) && Boolean(filter.nationality) && filter.nationality.map((item, index) => (
									<div
										className={classNames({
											"filters__option": true,
											"active": item.active,
										})}
										key={index}
										onClick={this.changeFilter.bind(this, "nationality", item.name)}
									>
										{item.name}
									</div>
								))}
							</div>
						</div>
						{/*/NATIONALITY*/}

						{/*SERVICES*/}
						{/* <div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Services</h3>

							<div className="filters__options-container">
								{Boolean(filter) && Boolean(filter.services) && filter.services.map((item, index) => (
									<div
										className={classNames({
											"filters__option": true,
											"active": item.active,
										})}
										key={index}
										onClick={this.changeFilter.bind(this, "services", item.name)}
									>
										{item.name}
									</div>
								))}
							</div>
						</div> */}
						{/*/SERVICES*/}

						{/*NEW SERVICES*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Services</h3>

							<div className="filters__options-container">
								{Boolean(filter) && Boolean(filter.services) && filter.services.map((item, index) => (
									<div
										className={classNames({
											"filters__option": true,
											"active": item.active,
										})}
										key={index}
										onClick={this.changeFilter.bind(this, "services", item.name)}
									>
										{item.name}
									</div>
								))}
							</div>
						</div>
						{/*/NEW SERVICES*/}

						{/*HAIR COLOUR*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Hair color</h3>
							{/*<a href="#" className="filters__section-reset js-reset-checkbox"/>*/}

							<div className="filters__options-container">
								{Boolean(filter) && Boolean(filter.hairColour) && filter.hairColour.map((item, index) => (
									<div
										className={classNames({
											"filters__option": true,
											"active": item.active,
										})}
										key={index}
										onClick={this.changeFilter.bind(this, "hairColour", item.name)}
									>
										{item.name}
									</div>
								))}
							</div>
						</div>
						{/*/HAIR COLOUR*/}

						{/*EYE COLOUR*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Eye color</h3>
							{/*<a href="#" className="filters__section-reset js-reset-checkbox"/>*/}

							<div className="filters__options-container">
								{Boolean(filter) && Boolean(filter.eyeColour) && filter.eyeColour.map((item, index) => (
									<div
										className={classNames({
											"filters__option": true,
											"active": item.active,
										})}
										key={index}
										onClick={this.changeFilter.bind(this, "eyeColour", item.name)}
									>
										{item.name}
									</div>
								))}
							</div>
						</div>
						{/*/EYE COLOUR*/}

						{/*SKIN COLOUR*/}
						<div className="filters__section js-filters-section">
							<h3 className="filters__section-title">Skin color</h3>
							{/*<a href="#" className="filters__section-reset js-reset-checkbox"/>*/}

							<div className="filters__options-container">
								{Boolean(filter) && Boolean(filter.skinColour) && filter.skinColour.map((item, index) => (
									<div
										className={classNames({
											"filters__option": true,
											"active": item.active,
										})}
										key={index}
										onClick={this.changeFilter.bind(this, "skinColour", item.name)}
									>
										{item.name}
									</div>
								))}
							</div>
						</div>
						{/*/SKIN COLOUR*/}

						{/*RESET*/}
						<div className="filters__reset">
							<button
								className="filters__reset-button"
								onClick={this.resetFilters}
							>
								<div className="filters__reset-container">
									Reset all settings
								</div>
							</button>
						</div>
						{/*/RESET*/}

					</div>
					{/*/FILTERS*/}
				</aside>

				<button
					className={classNames({
						"main-sidebar__close": true,
						"visible": this.state.sidebarOpen,
					})}
					onClick={this.closeSidebar}
				/>

				<RouteTransition
					pathname={this.props.location.pathname}
					{...transitionStyles}

				>
					{this.props.children}
				</RouteTransition>

				{this.props.loading > 0 && <LoadingSpinner/>}

				<div className="rotate-device"/>
			</div>
		);
	}
}

Panel.propTypes = {
	loading: PropTypes.number.isRequired,
	filter: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
	return {
		loading: state.ajaxCallsInProgress,
		filter : state.filter,
		sidebarOpen: state.sidebarOpen,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		girlActions: bindActionCreators(girlActions, dispatch),
		filterActions: bindActionCreators(filterActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Panel);
