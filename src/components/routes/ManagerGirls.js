// Core
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';

// Actions
import * as filterActions from '../../actions/filterActions';

// Misc
import classNames from 'classnames';

class Girls extends Component {

	changeFilter(filter, name) {
		this.props.filterActions.changeFilter(filter, name);
	}

	render() {

		let { girls, filter } = this.props;

		return (
			<div className="main-content main-content_shifted">
				<div className="main-content__container">

					{/*CATALOG TOP*/}
					<div className="catalog-top">

						<div/>

						{/*SORT*/}
						{1 < 0 &&
						<div className="catalog-top__sort">
							<div className="catalog-top__sort-name">Sort by:</div>
							<div className="catalog-top__sort-select">
								<select className="js-select">
									<option>Recommended</option>
									<option>Newest</option>
									<option>Oldest</option>
								</select>
							</div>
						</div>
						}
						{/*/SORT*/}

						{/*FILTER*/}
						<div className="catalog-top__filter">
						{Boolean(filter) && Boolean(filter.flags) && filter.flags.map((item, index) => (
							<div
								className={classNames({
									"catalog-top__filter-option": true,
									"active": item.active,
								})}
								key={index}
								onClick={this.changeFilter.bind(this, "flags", item.name)}
							>
								{item.name}
							</div>
						))}
						</div>
						{/*/FILTER*/}

					</div>
					{/*/CATALOG TOP*/}

					{/*TEXT GIRLS*/}
					<div className="text-girls">
						<div className="text-girls__list">

							{Boolean(girls) && girls.length > 0 && girls.map(item =>
								Boolean(!item.hidden) &&
								<Link to={"/manager/girls/" + item.id} className="text-girl" key={item.id}>
									<p className="text-girl__name">{item.title.rendered}</p>

									<div className="text-girl__flags">
										{Boolean(item['wpcf-new']) &&
										<div className="text-girl__flag text-girl__flag_red">New</div>
										}
										{Boolean(item['works-now']) &&
										<div className="text-girl__flag">Works now</div>
										}
									</div>

									<div className="text-girl__features">
										<div className="text-girl__feature">
											Age: {item['wpcf-age']}
										</div>
										<div className="text-girl__feature">
											Bust: {item['wpcf-bust-size']}
										</div>
										<div className="text-girl__feature">
											Height: {item['wpcf-height']}
										</div>
										<div className="text-girl__feature">
											Dress: {item['wpcf-dress']}
										</div>

										<div className="text-girl__feature-break"/>

										<div className="text-girl__feature">
											Hair color: {item['wpcf-hair-colour']}
										</div>
										<div className="text-girl__feature">
											Eye color: {item['wpcf-eye-colour']}
										</div>
										<div className="text-girl__feature">
											Skin color: {item['wpcf-skin-colour']}
										</div>
										<div className="text-girl__feature">
											Nationality: {item['wpcf-nationality']}
										</div>
									</div>
								</Link>
							)}

						</div>
					</div>
					{/*/TEXT GIRLS*/}
				</div>
			</div>
		)
	}
}

Girls.propTypes = {
	girls: PropTypes.array.isRequired,
	filter: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
	return {
		girls: state.girls,
		filter: state.filter,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		filterActions: bindActionCreators(filterActions, dispatch),
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Girls);