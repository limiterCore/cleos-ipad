// Core
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

// Misc
import { animateScroll } from 'react-scroll';
import classNames from 'classnames';

// Image loader
let Preload = require('react-preload').Preload;
let loadingIndicator = (
	<div
		className="girl__photo-manager girl__photo-manager_loading"
		style={{backgroundImage: 'url(/ajax-loader.gif)'}}
	/>
);

class ManagerGirl extends Component {
	constructor(props) {
		super(props);
		animateScroll.scrollToTop({
			duration: 2,
		});
	}

	render() {

		let girl = this.props.girls.find(el => el.id === parseInt(this.props.params.girlId, 10));
		if (!girl) {
			return false;
		}

		return (
			<div className="main-content main-content_shifted">
				<div className="main-content__container">

					{/*GIRL*/}
					<div className="girl">
						<div className="girl__title">
							<div className="girl__name">
								<button
									className="girl__back"
									onClick={browserHistory.goBack}
								/>
								{girl.title.rendered}
							</div>
							{Boolean(girl['wpcf-new']) &&
							<div className="girl__flag-new">new</div>
							}

							{Boolean(girl['works-now']) &&
							<div className="girl__flag-wt">works now</div>
							}
						</div>

						{/*PHOTOS*/}
						{Boolean(girl.gallery) && girl.gallery.length > 0 &&
						<Preload
							loadingIndicator={loadingIndicator}
							images={[girl.gallery[0]]}
							autoResolveDelay={3000}
							resolveOnError={true}
							mountChildren={true}
						>
							<div
								className="girl__photo-manager"
								style={{backgroundImage: 'url(' + girl.gallery[0] + ')'}}
							/>
						</Preload>
						}
						{/*/PHOTOS*/}

						{/*FEATURES*/}
						<div className="girl__features">
							<div className="girl__feature">
								<span className="girl__feature-title">Age</span>
								<span className="girl__feature-value">{girl['wpcf-age']}</span>
							</div>
							<div className="girl__feature">
								<span className="girl__feature-title">Bust</span>
								<span className="girl__feature-value">{girl['wpcf-bust-size']}</span>
							</div>
							<div className="girl__feature">
								<span className="girl__feature-title">Height</span>
								<span className="girl__feature-value">{girl['wpcf-height']} cm</span>
							</div>
							<div className="girl__feature">
								<span className="girl__feature-title">Dress</span>
								<span className="girl__feature-value">{girl['wpcf-dress']}</span>
							</div>
							<div className="girl__feature">
								<span className="girl__feature-title">Hair color</span>
								<span className="girl__feature-value">{girl['wpcf-hair-colour']}</span>
							</div>
							<div className="girl__feature">
								<span className="girl__feature-title">Eye color</span>
								<span className="girl__feature-value">{girl['wpcf-eye-colour']}</span>
							</div>
							<div className="girl__feature">
								<span className="girl__feature-title">Skin color</span>
								<span className="girl__feature-value">{girl['wpcf-skin-colour']}</span>
							</div>
							<div className="girl__feature">
								<span className="girl__feature-title">Nationality</span>
								<span className="girl__feature-value">{girl['wpcf-nationality']}</span>
							</div>
						</div>
						{/*/FEATURES*/}

						{/*ABOUT*/}
						{Boolean(girl.content) && Boolean(girl.content.rendered) &&
						<div className="girl__about">
							<h2 className="girl__middle-title">About me</h2>
							<div className="girl__text">
								{girl.content.rendered.replace(/<\/?[^>]+(>|$)/g, "")}
							</div>
						</div>
						}
						{/*/ABOUT*/}

						{/*SERVICES*/}
						{Boolean(this.props.filter.services) && this.props.filter.services.length > 0 &&
						<div className="girl__services">
							<h2 className="girl__middle-title">Services</h2>
							<div className="girl__services-list">
								{this.props.filter.services.map((item, index) => (
									<div
										className={classNames({
											"girl__service": true,
											"girl__service_active": girl.services.find(el => {
												return el.name === item.name && el.active;
											}),
										})}
										key={index}
									>
										<div className="girl__service-inner">
											{item.name}
										</div>
									</div>
								))}
							</div>
						</div>
						}
						{/*/SERVICES*/}

						{/*ROSTER*/}
						{Boolean(girl.roster) &&
						<div className="girl__roster">
							<h2 className="girl__middle-title">Roster</h2>
							<div className="girl__roster-list">
								{Object.keys(girl.roster).map((index, order) => (
									<div className="girl__roster-item" key={order}>
										<span className="girl__roster-item-days">{index}</span>
										<span className="girl__roster-item-value">{girl.roster[index][0]}</span>
									</div>
								))}
							</div>
						</div>
						}
						{/*/ROSTER*/}
					</div>
					{/*/GIRL*/}
				</div>
			</div>
		)
	}
}

ManagerGirl.PropTypes = {
	girls: PropTypes.array.isRequired,
	filter: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
	return {
		girls: state.girls,
		filter: state.filter,
	};
}

export default connect(mapStateToProps)(ManagerGirl);