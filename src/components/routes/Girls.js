// Core
import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { bindActionCreators } from 'redux';

// Actions
import * as girlActions from '../../actions/girlActions';

// Misc
const Entities = require('html-entities').AllHtmlEntities;
const entities = new Entities();

class Girls extends Component {

	constructor(props) {
		super(props);

		this.onLinkClick = this.onLinkClick.bind(this);
	}

	onLinkClick() {
		this.props.actions.openGirl();
	}

	render() {

		let girls = this.props.girls;

		return (
			<div className="main-content">
				<div className="main-content__container">

					{/*GIRLS*/}

					<div className="girls">
						<div className="girls__list">

							{Boolean(girls) && girls.length > 0 && girls.map(item =>
								Boolean(!item.hidden) &&
								<Link
									to={"/girls/" + item.id}
									className="girls__item"
									key={item.id}
								    onClick={this.onLinkClick}
								>
									<div className="girls__item-image" style={{backgroundImage: 'url(' + item.thumbnail[0] + ')'}}>
										<div className="girls__item-flags">
											{Boolean(item['wpcf-new']) &&
											<div className="girls__item-new">new</div>
											}
											{Boolean(item['works-now']) &&
											<div className="girls__item-wt">works now</div>
											}
										</div>
									</div>
									<p className="girls__item-name">{entities.decode(item.title.rendered)}</p>
									<div className="girls__item-features">
										<div className="girls__item-feature">
											<span className="girls__item-feature-title">Age</span>
											<span className="girls__item-feature-value">{item['wpcf-age']}</span>
										</div>
										<div className="girls__item-feature">
											<span className="girls__item-feature-title">Height</span>
											<span className="girls__item-feature-value">{item['wpcf-height']}</span>
										</div>
										<div className="girls__item-feature">
											<span className="girls__item-feature-title">Bust</span>
											<span className="girls__item-feature-value">{item['wpcf-bust-size']}</span>
										</div>
										<div className="girls__item-feature">
											<span className="girls__item-feature-title">Dress</span>
											<span className="girls__item-feature-value">{item['wpcf-dress']}</span>
										</div>
									</div>
								</Link>
							)}

						</div>
					</div>
					{/*/GIRLS*/}
				</div>
			</div>
		)
	}
}

Girls.propTypes = {
	girls: PropTypes.array.isRequired,
	actions: PropTypes.object.isRequired,
};

function mapStateToProps(state) {
	return {
		girls: state.girls,
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: bindActionCreators(girlActions, dispatch),
	}
}

export default connect(mapStateToProps, mapDispatchToProps)(Girls);