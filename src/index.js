import React from 'react';
import { render } from 'react-dom';

// Redux
import { Provider } from 'react-redux';
import configureStore from './store/configureStore';

// eslint-disable-next-line
import { Router, Route, IndexRoute, browserHistory } from 'react-router';

import './css/main.css';

// Components
import Panel from './components/routes/Panel';
import NotFound from './components/routes/NotFound';
import Girls from './components/routes/Girls';
import Girl from './components/routes/Girl';

import ManagerPanel from './components/routes/ManagerPanel';
import ManagerGirls from './components/routes/ManagerGirls';
import ManagerGirl from './components/routes/ManagerGirl';

const store = configureStore();

render(
	<Provider store={store}>
		<Router history={browserHistory}>
			<Route path="/" component={Panel}>
				<IndexRoute component={Girls}/>
				<Route path="girls/:girlId" component={Girl}/>
			</Route>
			<Route path="/manager/" component={ManagerPanel}>
				<IndexRoute component={ManagerGirls}/>
				<Route path="girls/:girlId" component={ManagerGirl}/>
			</Route>
			<Route path="*" component={NotFound} status={404}/>
		</Router>
	</Provider>,
	document.getElementById('root')
);
